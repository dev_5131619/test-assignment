from fastapi import APIRouter, Depends, HTTPException
from models import Employee, tokens_db, employees_db
from datetime import datetime

router = APIRouter()


@router.get('/salary')
def get_salary(employee: Employee = Depends(get_employee)):
    return {'salary': employee.salary}


@router.get('/next-increase-date')
def get_next_increase_date(employee: Employee = Depends(get_employee)):
    return {'next_increase_date': employee.next_increase_date}


def get_employee(token: str):
    if token not in tokens_db:
        raise HTTPException(status_code=401, detail='Invalid token')

    token_obj = tokens_db[token]
    if token_obj.expires_at < datetime.utcnow():
        raise HTTPException(status_code=401, detail='Token expired')

    employee_id = int(token.split('_')[1])
    return employees_db[employee_id]
