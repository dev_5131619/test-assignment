from fastapi import APIRouter, HTTPException
from models import Employee, Token, employees_db, tokens_db
from datetime import datetime, timedelta

router = APIRouter()

@router.post('/login')
def login(login: str, password: str):
    for employee in employees_db.values():
        if employee.login == login and employee.password == password:
            token = Token(token=f'token_{employee.id}', expires_at=datetime.utcnow() + timedelta(minutes=30))
            tokens_db[token.token] = token
            return {'token': token.token}
    raise HTTPException(status_code=401, detail='Invalid credentials')
