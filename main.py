from fastapi import FastAPI
from routers import authentication, salary

app = FastAPI()

app.include_router(authentication.router)
app.include_router(salary.router)