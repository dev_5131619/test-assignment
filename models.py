from pydantic import BaseModel
from datetime import datetime

class Employee(BaseModel):
    id: int
    login: str
    password: str
    salary: float
    next_increase_date: datetime

class Token(BaseModel):
    token: str
    expires_at: datetime

# In-memory database for simplicity
employees_db = {
    1: Employee(id=1, login='john', password='password1', salary=5000.0, next_increase_date=datetime(2023, 6, 30)),
    2: Employee(id=2, login='jane', password='password2', salary=6000.0, next_increase_date=datetime(2023, 7, 15)),
}

tokens_db = {}